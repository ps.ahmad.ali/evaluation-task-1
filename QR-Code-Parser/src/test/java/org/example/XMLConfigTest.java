package org.example;

import org.example.exceptions.UnsupportedFileTypeException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;

public class XMLConfigTest {

    @Test
    public void givenNullFile_whenCreatObject_thenExceptionIsThrown() {
        Assertions.assertThrows(NullPointerException.class, () -> new XMLConfig(null));
    }

    @Test
    public void givenNonExistingFile_whenCreatObject_thenExceptionIsThrown() {
        String filePath = "./file.xml";
        File file = new File(filePath);
        Assertions.assertThrows(FileNotFoundException.class, () -> new XMLConfig(file));
    }

    @Test
    public void givenNonXMLFile_whenCreatObject_thenExceptionIsThrown() {
        String filePath = "./st.txt";
        File file = new File(filePath);
        Exception thrown = Assertions.assertThrows(UnsupportedFileTypeException.class, () -> new XMLConfig(file));
        Assertions.assertEquals("not an XML file", thrown.getMessage());
    }

    @Test
    public void givenValidXMLFile_whenCreateObject_thenObjectIsCreated() {
        String filePath = "./QRStandard.xml";
        File file = new File(filePath);
        Assertions.assertDoesNotThrow(() -> new XMLConfig(file));
    }
}
