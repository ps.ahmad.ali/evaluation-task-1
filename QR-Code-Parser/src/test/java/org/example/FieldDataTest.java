package org.example;

import org.example.FieldData.FieldDataBuilder;
import org.example.enums.*;
import org.example.exceptions.InvalidFormatValue;
import org.example.exceptions.InvalidLengthValue;
import org.example.exceptions.InvalidPresenceValue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FieldDataTest {
    @Test
    public void givenNullParameters_whenCreateObject_thenExceptionIsThrown() {
        Exception thrown = Assertions.assertThrows(NullPointerException.class,
                () -> FieldDataBuilder.builder()
                        .setName(null)
                        .setFormat("N")
                        .setLength("02")
                        .setPresence("Mandatory")
                        .build());
        Assertions.assertEquals("name is null", thrown.getMessage());

        thrown = Assertions.assertThrows(NullPointerException.class,
                () -> FieldDataBuilder.builder()
                        .setName("id")
                        .setFormat(null)
                        .setLength("02")
                        .setPresence("Mandatory")
                        .build());
        Assertions.assertEquals("format is null", thrown.getMessage());

        thrown = Assertions.assertThrows(NullPointerException.class,
                () -> FieldDataBuilder.builder()
                        .setName("id")
                        .setFormat("N")
                        .setLength(null)
                        .setPresence("Mandatory")
                        .build());
        Assertions.assertEquals("length is null", thrown.getMessage());

        thrown = Assertions.assertThrows(NullPointerException.class,
                () -> FieldDataBuilder.builder()
                        .setName("id")
                        .setFormat("N")
                        .setLength("02")
                        .setPresence(null)
                        .build());
        Assertions.assertEquals("presence is null", thrown.getMessage());

    }

    @Test
    public void givenInvalidParameters_whenCreateObject_thenExceptionIsThrown() {
        Exception thrown = Assertions.assertThrows(InvalidPresenceValue.class, () -> FieldDataBuilder.builder()
                .setName("a")
                .setFormat("N")
                .setLength("02")
                .setPresence("M")
                .build());
        Assertions.assertEquals("presence should be Mandatory or Optional, not M", thrown.getMessage());

        thrown = Assertions.assertThrows(InvalidFormatValue.class, () -> FieldDataBuilder.builder()
                .setName("id")
                .setFormat("String")
                .setLength("02")
                .setPresence("Mandatory")
                .build());
        Assertions.assertEquals("format should be N,S or ans, not String", thrown.getMessage());

        thrown = Assertions.assertThrows(InvalidLengthValue.class, () -> FieldDataBuilder.builder()
                .setName("id")
                .setFormat("N")
                .setLength("A4")
                .setPresence("Mandatory")
                .build());
        Assertions.assertEquals("length should only contain numeric values or ranges", thrown.getMessage());
    }

    @Test
    public void given_when_then() {
        FieldData field = FieldDataBuilder.builder()
                .setName("Method")
                .setFormat("S")
                .setLength("0-50")
                .setPresence("Mandatory")
                .build();
        Assertions.assertEquals("Method", field.getName());
        Assertions.assertEquals(Format.valueOf("S"), field.getFormat());
        Assertions.assertEquals("0-50", field.getLength());
        Assertions.assertEquals(Presence.valueOf("MANDATORY"), field.getPresence());
    }
}
