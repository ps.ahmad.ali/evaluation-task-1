package org.example;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

public class QRTextChecksumTest {

    @Test
    public void givenNullQRCodeText_whenGenerateChecksum_thenResultIsReturned() {
        QRTextChecksum check = new QRTextChecksum();
        Assertions.assertThrows(NullPointerException.class, () -> check.generateChecksum(null));
    }

    @Test
    public void givenQRCodeText_whenGenerateChecksum_thenResultIsReturned() {
        QRTextChecksum check = new QRTextChecksum();
        String qrCodeText = "00020101021228270004CliQ0108TESTJOA0" +
                "0203002520412345303400540515.1055020256050.900" +
                "5802JO5910Merchant026005Amman61051111862800103" +
                "0020306store20404loy20510QRTest02540605cust207" +
                "04ter20804JOQR100500222110340064180002FR0108Ma" +
                "rchand80310004JOQR01192021-04-05T10:47:576304";
        Assertions.assertEquals("1E63", check.generateChecksum(qrCodeText));
    }

}
