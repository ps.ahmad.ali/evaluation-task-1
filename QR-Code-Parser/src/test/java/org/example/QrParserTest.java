package org.example;

import org.example.exceptions.*;
import org.jdom2.JDOMException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class QrParserTest {

    @Test
    public void givenNullConfigObject_whenCreateObject_thenExceptionIsThrown() {
        Exception thrown = Assertions.assertThrows(NullPointerException.class, () -> new QrParser(null));
        Assertions.assertEquals("Config object is null", thrown.getMessage());
    }

    @Test
    public void givenNullQRText_whenParse_thenExceptionIsThrown() throws IOException, JDOMException {
        String filePath = "./QRStandard.xml";
        File file = new File(filePath);
        QrParser parser = new QrParser(new XMLConfig(file));
        Assertions.assertThrows(NullPointerException.class, () -> parser.parse(null));
    }

    @Test
    public void givenInvalidQrText_whenParse_thenExceptionIsThrown() throws IOException, JDOMException {
        String filePath = "./QRStandard.xml";
        File file = new File(filePath);
        QrParser parser = new QrParser(new XMLConfig(file));
        String qrText = "0484512";
        Exception thrown = Assertions.assertThrows(InvalidInputException.class, () -> parser.parse(qrText));
        Assertions.assertEquals("QR Text must be at least 50 characters long to have all the mandatory fields", thrown.getMessage());
    }

    @Test
    public void givenQRTextWithInvalidCRCValue_whenParse_thenExceptionIsThrown() throws IOException, JDOMException {
        String filePath = "./QRStandard.xml";
        File file = new File(filePath);
        QrParser parser = new QrParser(new XMLConfig(file));
        String qrText = "00020101021226280005JOMOP0115ARABJO0000050" +
                "0128540004CliQ0108ARABJOA00230JO61ARAB1340000000" +
                "13403179510052045122530340054056.5235802JO5915AB" +
                " Grand Stores6005Amman6261010722876910315ARABJO0" +
                "00005001050722876910708005001010804JOQR80310004J" +
                "OQR01192021-02-20T10:05:236304FA5A";
        Assertions.assertThrows(InvalidCRCValueException.class, () -> parser.parse(qrText));
    }

    @Test
    public void givenQRTextWithInvalidLengths_whenParse_thenExceptionIsThrown() throws IOException, JDOMException {
        String filePath = "./QRStandard.xml";
        File file = new File(filePath);
        QrParser parser = new QrParser(new XMLConfig(file));
        String qrText1 = "00070101021226280005JOMOP0115ARABJO0000050" +
                "0128540004CliQ0108ARABJOA00230JO61ARAB1340000000" +
                "13403179510052045122530340054056.5235802JO5915AB" +
                " Grand Stores6005Amman6261010722876910315ARABJO0" +
                "00005001050722876910708005001010804JOQR80310004J" +
                "OQR01192021-02-20T10:05:2363042BCA";
        Exception thrown = Assertions.assertThrows(InvalidLengthException.class, () -> parser.parse(qrText1));
        Assertions.assertEquals("element ID should have a length of 2", thrown.getMessage());

        String qrText2 = "00020101021226280005JOMOP0115ARABJO0000050" +
                "0128540004CliQ0108ARABJOA00230JO61ARAB1340000000" +
                "13403179510052045122530340054056.5235802JO5915AB" +
                " Grand Stores6005Amman6261010722876910315ARABJO0" +
                "00005001050722876910708005001010804JOQR80310004J" +
                "OQR01342021-02-20T10:05:236304320E";
        thrown = Assertions.assertThrows(InvalidLengthException.class, () -> parser.parse(qrText2));
        Assertions.assertEquals("element qr date should have a length of 0-32", thrown.getMessage());

    }

    @Test
    public void givenQRCodeWithInvalidFieldFormat_whenParse_thenExceptionIsThrown() throws IOException, JDOMException {
        String filePath = "./QRStandard.xml";
        File file = new File(filePath);
        QrParser parser = new QrParser(new XMLConfig(file));
        String qrText1 = "0002a201021226280005JOMOP0115ARABJO0000050" +
                "0128540004CliQ0108ARABJOA00230JO61ARAB1340000000" +
                "13403179510052045122530340054056.5235802JO5915AB" +
                " Grand Stores6005Amman6261010722876910315ARABJO0" +
                "00005001050722876910708005001010804JOQR80310004J" +
                "OQR01192021-02-20T10:05:2363042E64";
        Exception thrown = Assertions.assertThrows(InvalidFormatException.class, () -> parser.parse(qrText1));
        Assertions.assertEquals("ID has the wrong format", thrown.getMessage());
    }


    @Test
    public void givenValidQrText_whenParse_thenFieldsAreReturned() throws IOException, JDOMException {
        String filePath = "./QRStandard.xml";
        File file = new File(filePath);
        XMLConfig config = new XMLConfig(file);
        QrParser parser = new QrParser(config);
        String qrCodeText = "00020101021226280005JOMOP0115ARABJ" +
                "O00000500128540004CliQ0108ARABJOA00230JO61ARAB" +
                "134000000013403179510052045122530340054056.523" +
                "5802JO5915AB Grand Stores6005Amman626101072287" +
                "6890315ARABJO000005001050722876890708005001010" +
                "804JOQR80310004JOQR01192021-02-20T10:03:3363041BAB";
        HashMap<String, String> parsedFields = parser.parse(qrCodeText);
        Assertions.assertEquals("01", parsedFields.get("00"));
        Assertions.assertEquals("1BAB", parsedFields.get("63"));
        qrCodeText = "00020101021228270004CliQ0108TESTJOA0" +
                "0203002520412345303400540515.1055020256050.900" +
                "5802JO5910Merchant026005Amman61051111862800103" +
                "0020306store20404loy20510QRTest02540605cust207" +
                "04ter20804JOQR100500222110340064180002FR0108Ma" +
                "rchand80310004JOQR01192021-04-05T10:47:5763041E63";
        parsedFields = parser.parse(qrCodeText);
        Assertions.assertEquals("01", parsedFields.get("00"));
        Assertions.assertEquals("1E63", parsedFields.get("63"));

    }
}

