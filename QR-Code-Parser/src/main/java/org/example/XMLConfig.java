package org.example;

import org.example.exceptions.UnsupportedFileTypeException;
import org.example.FieldData.FieldDataBuilder;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class XMLConfig {

    private final HashMap<String, FieldData> fieldsConfig = new HashMap<>();
    private final HashMap<String, Boolean> compositeChecker = new HashMap<>();


    public XMLConfig(File configFile) throws IOException, JDOMException {
        if (configFile == null)
            throw new NullPointerException();
        String fileType = configFile.getName().split("\\.")[1];
        if (!fileType.equals("xml"))
            throw new UnsupportedFileTypeException("not an XML file");
        if (!configFile.exists())
            throw new FileNotFoundException();
        parseConfig(configFile);
    }

    private void parseConfig(File configFile) throws IOException, JDOMException {
        SAXBuilder sax = new SAXBuilder();
        Document doc = sax.build(configFile);

        Element rootNode = doc.getRootElement();
        parseAllFields(rootNode, "");
    }

    private void parseAllFields(Element rootNode, String parentId) {
        List<Element> fields = rootNode.getChildren("Field");
        for (Element field : fields) {
            String id = parentId + field.getAttributeValue("id");
            putField(field, id);
            List<Element> subFields = field.getChildren("Field");
            if (subFields.isEmpty()) {
                compositeChecker.put(id, false);
                continue;
            }
            compositeChecker.put(id, true);
            parseAllFields(field, id);
        }
    }

    private void putField(Element field, String id) {
        String name = field.getAttributeValue("name");
        String presence = field.getAttributeValue("presence");
        String length = field.getAttributeValue("length");
        String format = field.getAttributeValue("format");
        fieldsConfig.put(id, FieldDataBuilder.builder()
                .setName(name)
                .setFormat(format)
                .setLength(length)
                .setPresence(presence)
                .build());
    }


    public HashMap<String, FieldData> getConfig() {
        return fieldsConfig;
    }


    public boolean isComposite(String id) {
        return compositeChecker.get(id);
    }
}
