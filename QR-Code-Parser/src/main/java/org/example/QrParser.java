package org.example;

import org.example.exceptions.*;

import java.util.HashMap;

public class QrParser {

    class QrTextFieldParser {
        private String idText;
        private String lengthText;
        private String valueText;
        private final String qrText;

        public QrTextFieldParser(String qrText) {
            this.qrText = qrText;
        }

        public void parseField(int index, String parentId) {
            int startIndex = index;

            String idText = parentId + qrText.substring(startIndex, (startIndex += 2));
            String lengthText = qrText.substring(startIndex, (startIndex += 2));
            checkLength(idText, lengthText);
            String valueText = qrText.substring(startIndex, startIndex + Integer.parseInt(lengthText));
            checkFormat(idText, valueText);

            this.idText = idText;
            this.lengthText = lengthText;
            this.valueText = valueText;
        }

        public String getIdText() {
            return idText;
        }

        public String getLengthText() {
            return lengthText;
        }

        public String getValueText() {
            return valueText;
        }
    }

    private final XMLConfig config;
    private final HashMap<String, FieldData> fieldsConfig;

    public QrParser(XMLConfig standardConfig) {

        if (standardConfig == null)
            throw new NullPointerException("Config object is null");
        config = standardConfig;
        fieldsConfig = standardConfig.getConfig();

    }

    public HashMap<String, String> parse(String qrText) {
        if (qrText == null)
            throw new NullPointerException();
        if (qrText.length() < 50)
            throw new InvalidInputException("QR Text must be at least 50 characters long to have all the mandatory fields");
        if (!isValidCRCValue(qrText))
            throw new InvalidCRCValueException();

        return parseAllFields(qrText, "");
    }

    private HashMap<String, String> parseAllFields(String qrText, String parentId) {
        QrTextFieldParser field = new QrTextFieldParser(qrText);
        HashMap<String, String> parsed = new HashMap<>();

        for (int i = 0; i < qrText.length(); ) {

            field.parseField(i, parentId);
            i += (4 + Integer.parseInt(field.getLengthText()));
            if (config.isComposite(field.getIdText())) {
                parsed.putAll(parseAllFields(field.getValueText(), field.getIdText()));
                continue;
            }
            parsed.put(field.getIdText(), field.getValueText());
        }
        return parsed;
    }

    private void checkFormat(String id, String field) {
        if (!isValidFormat(id, field))
            throw new InvalidFormatException(fieldsConfig.get(id).getName() + " has the wrong format");
    }

    private boolean isValidFormat(String id, String field) {
        String format = fieldsConfig.get(id).getFormat().toString();
        if (format.equals("S") || format.equals("ANS"))
            return true;
        return format.equals("N") && field.matches("^[0-9]+$");
    }

    private void checkLength(String id, String textLength) {
        if (!isValidLength(id, textLength))
            throw new InvalidLengthException("element " + fieldsConfig.get(id).getName() + " should have a length of " + fieldsConfig.get(id).getLength());
    }

    private boolean isValidCRCValue(String qrText) {
        QRTextChecksum check = new QRTextChecksum();
        String crcValue = check.generateChecksum(qrText.substring(0, (qrText.length() - 4)));
        String expectedCRCValue = qrText.substring((qrText.length() - 4));
        return crcValue.equals(expectedCRCValue);
    }

    private boolean isValidLength(String id, String length) {
        String validLength = fieldsConfig.get(id).getLength();
        if (validLength.length() >= 3) {
            int minLength = Integer.parseInt(validLength.split("-")[0]);
            int maxLength = Integer.parseInt(validLength.split("-")[1]);
            return Integer.parseInt(length) >= minLength && Integer.parseInt(length) <= maxLength;
        }
        return Integer.parseInt(validLength) == Integer.parseInt(length);
    }
}



