package org.example.exceptions;

public class InvalidFormatValue extends RuntimeException {
    public InvalidFormatValue(String message) {
        super(message);
    }
}
