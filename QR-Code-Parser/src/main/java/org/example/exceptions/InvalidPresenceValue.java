package org.example.exceptions;

public class InvalidPresenceValue extends RuntimeException {
    public InvalidPresenceValue(String message) {
        super(message);
    }
}
