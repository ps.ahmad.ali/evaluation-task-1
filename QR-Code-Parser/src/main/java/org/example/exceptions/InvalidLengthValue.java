package org.example.exceptions;

public class InvalidLengthValue extends RuntimeException {
    public InvalidLengthValue(String message) {
        super(message);
    }
}
