package org.example;

import org.example.enums.*;
import org.example.exceptions.InvalidFormatValue;
import org.example.exceptions.InvalidLengthValue;
import org.example.exceptions.InvalidPresenceValue;

import java.util.Locale;

public class FieldData {

    interface NameStep {
        FormatStep setName(String name);
    }

    interface FormatStep {
        LengthStep setFormat(String format);
    }

    interface LengthStep {
        PresenceStep setLength(String length);
    }

    interface PresenceStep {
        BuildStep setPresence(String presence);
    }

    interface BuildStep {
        FieldData build();
    }

    public static class FieldDataBuilder implements NameStep, FormatStep, LengthStep, PresenceStep, BuildStep {
        private String name;
        private Format format;
        private String length;
        private Presence presence;

        public static NameStep builder() {
            return new FieldDataBuilder();
        }

        private FieldDataBuilder() {
        }

        @Override
        public FormatStep setName(String name) {
            if (name == null)
                throw new NullPointerException("name is null");
            this.name = name;
            return this;
        }

        @Override
        public LengthStep setFormat(String format) {
            if (format == null)
                throw new NullPointerException("format is null");
            try {
                this.format = Format.valueOf(format.toUpperCase(Locale.ROOT));
            } catch (IllegalArgumentException e) {
                throw new InvalidFormatValue("format should be N,S or ans, not " + format);
            }
            return this;
        }

        @Override
        public PresenceStep setLength(String length) {
            if (length == null)
                throw new NullPointerException("length is null");
            if (!length.matches("^[\\d-]+$"))
                throw new InvalidLengthValue("length should only contain numeric values or ranges");
            this.length = length;
            return this;
        }

        @Override
        public BuildStep setPresence(String presence) {
            if (presence == null)
                throw new NullPointerException("presence is null");
            try {
                this.presence = Presence.valueOf(presence.toUpperCase(Locale.ROOT));
            } catch (IllegalArgumentException e) {
                throw new InvalidPresenceValue("presence should be Mandatory or Optional, not " + presence);
            }
            return this;
        }

        @Override
        public FieldData build() {
            return new FieldData(name, format, length, presence);
        }
    }

    private final String name;
    private final Format format;
    private final String length;
    private final Presence presence;

    private FieldData(String name, Format format, String length, Presence presence) {
        this.name = name;
        this.format = format;
        this.length = length;
        this.presence = presence;
    }

    public String getName() {
        return name;
    }

    public Format getFormat() {
        return format;
    }

    public String getLength() {
        return length;
    }

    public Presence getPresence() {
        return presence;
    }
}
