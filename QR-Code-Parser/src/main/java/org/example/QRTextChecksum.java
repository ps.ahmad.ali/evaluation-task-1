package org.example;

import java.nio.charset.StandardCharsets;

public class QRTextChecksum {

    public String generateChecksum(String Text) {
        if (Text == null)
            throw new NullPointerException();

        int checksum = 0xffff;
        int polynomial = 0x1021;

        byte[] data = Text.getBytes(StandardCharsets.UTF_8);
        for (byte b : data) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b >> (7 - i) & 1) == 1);
                boolean c15 = ((checksum >> 15 & 1) == 1);
                checksum <<= 1;
                if (c15 ^ bit) {
                    checksum ^= polynomial;
                }
            }
        }
        checksum &= 0xffff;

        return Integer.toHexString(checksum).toUpperCase();
    }
}
